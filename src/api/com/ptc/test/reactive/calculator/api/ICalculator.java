package com.ptc.test.reactive.calculator.api;

import java.lang.reflect.Type;

import com.google.common.reflect.TypeToken;
import com.ptc.microservices.ServiceException;
import com.ptc.microservices.reactive.JsonSerializable;
import com.ptc.microservices.reactive.ServiceMessage;

public interface ICalculator
{
	public ExprResult add (ExprParams expr) throws ServiceException, InterruptedException;
	public ExprResult subtract (ExprParams expr) throws ServiceException, InterruptedException;
	public ExprResult multiply (ExprParams expr) throws ServiceException, InterruptedException;
	public ExprResult divide (ExprParams expr) throws ServiceException, InterruptedException;
	
	// ----------------------------------------------------------	
	public static class ExprParams implements JsonSerializable
	{
		private int left;
		private int right;
		
		public ExprParams (int left, int right)
		{
			super ();
			this.left = left;
			this.right = right;
		}
		
		public int getLeft ()
		{
			return left;
		}
		public void setLeft (int left)
		{
			this.left = left;
		}
		public int getRight ()
		{
			return right;
		}
		public void setRight (int right)
		{
			this.right = right;
		}
		public static final Type TYPE = new TypeToken<ServiceMessage<ExprParams>>() {}.getType ();
		//public static final Type M_TYPE = new MessageType<ExprParams>() {}.getType ();
	}
	
	public static class ExprResult implements JsonSerializable
	{
		private int value;

		public ExprResult (int value)
		{
			super ();
			this.value = value;
		}

		public int getValue ()
		{
			return value;
		}

		public void setValue (int value)
		{
			this.value = value;
		}
		
//		public static final Type BasicServiceResponse_TYPE = new TypeToken<BasicServiceResponse<ExprResult>>() {}.getType ();
		public static final Type TYPE = new TypeToken<ServiceMessage<ExprResult>>() {}.getType ();
		//public static final Type M_TYPE = new MessageType<ExprResult>() {}.getType ();

//		public static final Type ServiceRequest_TYPE = new TypeToken<ServiceRequest<ExprParams>>() {}.getType ();
	}
}
