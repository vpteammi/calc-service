package com.ptc.test.reactive.calculator.clients;

import com.ptc.ccp.vertxservices.reactive.EbusReactiveConnector;
import com.ptc.test.reactive.calculator.api.ICalculator.ExprParams;
import com.ptc.test.reactive.calculator.api.ICalculator.ExprResult;
import com.sm.javax.utilx.ApplicationProperties;

public class EbusCalculatorClient 
{

	@FunctionalInterface
	public interface FuncWithExc<T, R, E extends Throwable> {
		R apply(T p) throws E;
	}

	public static void test(String op, FuncWithExc<ExprParams, ExprResult, Throwable> func, int left, int right) {
		try {
			ExprResult res = func.apply(new ExprParams(left, right));
			System.out.println(left + " " + op + " " + right + " = " + res.getValue());
		} catch (Throwable err) {
			printError(err, false);
		}
	}

	public static void printError (Throwable err, boolean stack) {
		System.out.println("Error: " + (err != null ? err.getMessage() : "unknown"));
		if (stack) {
			err.printStackTrace();
		}
	}

	public static void main(String[] args) {
		ApplicationProperties.loadProperties(null, args, EbusCalculatorClient.class);
		try {
			EbusReactiveConnector.connectToServer ("ebus-calculator", CalculatorClient.class, calculator -> {
				test("+", calculator::add, 10, 17);
				test("*", calculator::multiply, 10, 17);
				test("/", calculator::divide, 13, 0);
				test("-", calculator::subtract, 10, 17);
				calculator.release ();
			}, failure -> {
				printError(failure, false);
			});
		} catch (NoSuchMethodException | SecurityException e) {
			printError(e, false);
		}
	}

}
