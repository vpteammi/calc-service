package com.ptc.test.reactive.calculator.clients;

import java.util.function.Consumer;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.ptc.microservices.ServiceException;
import com.ptc.microservices.reactive.IReactiveConnector;
import com.ptc.microservices.reactive.ReactiveClient;
import com.ptc.microservices.reactive.ServiceResponse;
import com.ptc.test.reactive.calculator.api.ICalculator;

public class CalculatorClient extends ReactiveClient<CalculatorClient> implements ICalculator
{

	public CalculatorClient (IReactiveConnector connection)
	{
		super (connection);
		// TODO Auto-generated constructor stub
	}

	// ----------------------------------------------------------
	@Override
	public ExprResult add (ExprParams expr) throws ServiceException, InterruptedException
	{
		return synchronize (c -> add (expr, c));
	}

	@Override
	public ExprResult subtract (ExprParams expr) throws ServiceException, InterruptedException
	{
		return synchronize (c -> subtract (expr, c));
	}

	@Override
	public ExprResult multiply (ExprParams expr) throws ServiceException, InterruptedException
	{
		return synchronize (c -> multiply (expr, c));
	}

	@Override
	public ExprResult divide (ExprParams expr) throws ServiceException, InterruptedException
	{
		return synchronize (c -> divide (expr, c));
	}

	// ----------------------------------------------------------
	private Gson gson = new GsonBuilder ().create ();

	public void add (ExprParams expr, Consumer<ServiceResponse<ExprResult>> resultHandler)
	{
		send ("add", expr, resultHandler, ExprResult.TYPE /*this::readResponse*/);
	}

	public void subtract (ExprParams expr, Consumer<ServiceResponse<ExprResult>> resultHandler)
	{
		send ("subtract", expr, resultHandler, ExprResult.TYPE);
	}

	public void multiply (ExprParams expr, Consumer<ServiceResponse<ExprResult>> resultHandler)
	{
		send ("multiply", expr, resultHandler, ExprResult.TYPE);
	}

	public void divide (ExprParams expr, Consumer<ServiceResponse<ExprResult>> resultHandler)
	{
		send ("divide", expr, resultHandler, ExprResult.TYPE);
	}

}
