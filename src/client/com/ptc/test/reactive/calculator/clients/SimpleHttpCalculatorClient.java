package com.ptc.test.reactive.calculator.clients;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.ptc.microservices.clients.JsonHttpClient;
import com.ptc.microservices.clients.SimpleHttpReactiveConnector;
import com.ptc.test.reactive.calculator.api.ICalculator.ExprParams;
import com.ptc.test.reactive.calculator.api.ICalculator.ExprResult;
import com.sm.javax.langx.Strings;
import com.sm.javax.utilx.ApplicationProperties;
import com.sm.javax.utilx.CommandLineArguments;

public class SimpleHttpCalculatorClient {

	@FunctionalInterface
	public interface FuncWithExc<T, R, E extends Throwable> {
		R apply(T p) throws E;
	}

	public static void test(String op, FuncWithExc<ExprParams, ExprResult, Throwable> func, int left, int right) {
		try {
			ExprResult res = func.apply(new ExprParams(left, right));
			System.out.println(left + " " + op + " " + right + " = " + res.getValue());
		} catch (Throwable err) {
			printError(err, false);
		}
	}

	public static void printError (Throwable err, boolean stack) {
		System.out.println("Error: " + err.getMessage());
		if (stack) {
			err.printStackTrace();
		}
	}

	public SimpleHttpCalculatorClient() {
		super ();
	}

	public static String getAddress (ApplicationProperties props, String[] args, String cmdLineArg)
	{
		URL url = null;
		CommandLineArguments cmdArgs = null;
		if (props != null) {
			cmdArgs = props.getCommandLineArgs ();
		}
		else {
			cmdArgs = new CommandLineArguments (args);
		}
		
		if (cmdArgs != null) {
			String serverAddr = cmdArgs.getArgument (cmdLineArg);
			if (! Strings.isEmpty (serverAddr)) {
				try {
					url = new URL (serverAddr);
				} 
				catch (MalformedURLException e) {
					System.out.println ("Error: command line argument '-" + cmdLineArg + "=" + serverAddr + "' is not a valid URL");
				}
			} 
		}

		return (url != null ? url.toString() : null);
	}

	public static String getDiscoveryServerAddress (ApplicationProperties props, String[] args)
	{
		return getAddress (props, args, "discovery");
	}

	public static String getServerAddress (ApplicationProperties props, String[] args)
	{
		return getAddress (props, args, "server");
	}
	
	private static List<String> getAddresses (JsonObject response)
	{
		List<String> addresses = null;
		JsonElement success = response.get ("success"); 
		if (success != null && success.isJsonPrimitive () && success.getAsBoolean ()) {
			JsonElement result = response.get ("result");
			if (result != null && result.isJsonArray ()) {
				JsonArray records = result.getAsJsonArray ();
				addresses = new ArrayList<String> (records.size ());
				for (JsonElement loc : records) {
					addresses.add (loc.getAsJsonObject ().get ("location").getAsJsonObject().get ("endpoint").getAsString ());
				}
			}
		}
		return addresses;
	}
	
	public static String findServerAddress (ApplicationProperties props, String[] args)
	{
		String address = getServerAddress (props, args);
		if (address == null) {
			String discoveryServer = getDiscoveryServerAddress (props, args);
			if (discoveryServer != null) {
				JsonHttpClient client;
				try {
					client = new JsonHttpClient (new URL (discoveryServer), 5000);
					JsonObject request = new JsonObject ();
					request.addProperty ("command", "find");
					request.addProperty ("service-name", "web-calculator");
					request.addProperty ("service-type", "http-endpoint");
					CompletableFuture<String> future = new CompletableFuture<String> ();
					client.sendPostRequest (request, res -> {
						if (res.succeeded ()) {
							JsonObject response = res.value ();
							System.out.println ("Discovery service response:\n " + response);
							List<String> addrs = getAddresses (response);
							if (addrs != null && addrs.size() > 0) {
								future.complete(addrs.get (0));
							}
						}
						else {
							future.completeExceptionally (res.cause ());
						}
					});
					address = future.get (5000, TimeUnit.MILLISECONDS);
				} 
				catch (MalformedURLException | InterruptedException | ExecutionException | TimeoutException e) {
					System.out.println ("Error: " + e.getMessage ());
				}
			}
		}
		return address;
	}
	
	private static void usage ()
	{
		System.err.println ("Command line argument -server is missed or invalid.");
		System.out.println ("Please use: -server=<calc-server-url>");
		System.out.println ("        or: -discovery=<discovery-server-url>");
		System.exit (1);
	}

	public static void main (String[] args) 
	{
		ApplicationProperties props = ApplicationProperties.loadProperties(null, args, SimpleHttpCalculatorClient.class);
		try {
			String address = findServerAddress (props, args);
			if (address == null) {
				usage ();
			}
			SimpleHttpReactiveConnector.connectToServer (address, CalculatorClient.class, calculator -> {
				if (calculator != null) {
					test("+", calculator::add, 10, 17);
					test("*", calculator::multiply, 10, 17);
					test("/", calculator::divide, 13, 0);
					test("-", calculator::subtract, 10, 17);
					calculator.release ();
					System.exit (0);
				}
			}, failure -> {
				printError(failure, false);
				System.exit (1);
			});
		} catch (NoSuchMethodException | SecurityException e) {
			printError(e, false);
		}
	}

}
