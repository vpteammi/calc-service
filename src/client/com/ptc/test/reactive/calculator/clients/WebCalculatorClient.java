package com.ptc.test.reactive.calculator.clients;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.function.Consumer;

import com.ptc.ccp.vertxservices.reactive.HttpReactiveConnector;
import com.ptc.test.reactive.calculator.api.ICalculator.ExprParams;
import com.ptc.test.reactive.calculator.api.ICalculator.ExprResult;
import com.sm.javax.langx.Strings;
import com.sm.javax.utilx.ApplicationProperties;
import com.sm.javax.utilx.CommandLineArguments;

public class WebCalculatorClient {

	@FunctionalInterface
	public interface FuncWithExc<T, R, E extends Throwable> {
		R apply(T p) throws E;
	}

	public static void test(String op, FuncWithExc<ExprParams, ExprResult, Throwable> func, int left, int right) {
		try {
			ExprResult res = func.apply(new ExprParams(left, right));
			System.out.println(left + " " + op + " " + right + " = " + res.getValue());
		} catch (Throwable err) {
			printError(err, false);
		}
	}

	public static void printError (Throwable err, boolean stack) {
		System.out.println("Error: " + (err != null ? err.getMessage() : "unknown"));
		if (stack) {
			err.printStackTrace();
		}
	}

	public WebCalculatorClient() {
		super ();
	}
	
	public static URL getUrl (ApplicationProperties props, String[] args)
	{
		URL url = null;
		CommandLineArguments cmdArgs = null;
		if (props != null) {
			cmdArgs = props.getCommandLineArgs ();
		}
		else {
			cmdArgs = new CommandLineArguments (args);
		}
		
		if (cmdArgs != null) {
			String serverAddr = cmdArgs.getArgument ("server");
			if (! Strings.isEmpty (serverAddr)) {
				try {
					url = new URL (serverAddr);
				} 
				catch (MalformedURLException e) {
					System.out.println ("Error: command line argument '-server=" + serverAddr + "' is not a valid URL");
				}
			} 
		}
		
		return url;
	}

	public static void main(String[] args) {
		ApplicationProperties props = ApplicationProperties.loadProperties(null, args, WebCalculatorClient.class);
		try {
			Consumer<CalculatorClient> jobProcessor = calculator -> {
				test("+", calculator::add, 10, 17);
				test("*", calculator::multiply, 10, 17);
				test("/", calculator::divide, 13, 0);
				test("-", calculator::subtract, 10, 17);
				calculator.release ();
			};
			URL serverUrl = getUrl (props, args);
			if (serverUrl != null) {
				HttpReactiveConnector.connectToServer (serverUrl, CalculatorClient.class, jobProcessor, failure -> {
					printError(failure, false);
				});
			}
			else {
				HttpReactiveConnector.connectToServer ("web-calculator", CalculatorClient.class, jobProcessor, failure -> {
					printError(failure, false);
				});
			}
		} catch (NoSuchMethodException | SecurityException e) {
			printError(e, false);
		}
	}

}
