package com.ptc.test.reactive.calculator.server;

import com.ptc.ccp.vertxservices.reactive.VertxReactiveServer;
import com.ptc.microservices.reactive.ReactiveService;
import com.ptc.test.reactive.calculator.api.ICalculator;

public class EbusCalculatorServer extends ReactiveService implements ICalculator
{
	public EbusCalculatorServer ()
	{
		super ();
		this.registerMethod ("add", this::add, ExprParams.TYPE)
			.registerMethod ("subtract", this::subtract, ExprParams.TYPE)
			.registerMethod ("multiply", this::multiply, ExprParams.TYPE)
			.registerMethod ("divide", this::divide, ExprParams.TYPE);
	}

	// --------------------------------------------------------------------------
	@Override
	public ExprResult add (ExprParams expr)
	{
		return new ExprResult (expr.getLeft () + expr.getRight ());
	}


	@Override
	public ExprResult subtract (ExprParams expr)
	{
		return new ExprResult (expr.getLeft () - expr.getRight ());
	}


	@Override
	public ExprResult multiply (ExprParams expr)
	{
		return new ExprResult (expr.getLeft () * expr.getRight ());
	}


	@Override
	public ExprResult divide (ExprParams expr)
	{
		return new ExprResult (expr.getLeft () / expr.getRight ());
	}

	// --------------------------------------------------------------------------
	public static void main (String[] args)
	{
		VertxReactiveServer server = VertxReactiveServer.createInstance (args);
		ReactiveService calculator = new EbusCalculatorServer ();
		server.registerEventBusService ("ebus-calculator", calculator /* new EbusCalculatorServer () */);
		server.registerWebService ("web-calculator", 8080, "/wcalc", calculator /* new EbusCalculatorServer () */);
		server.start ();
	}

}
